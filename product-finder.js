﻿var finder = {
	init : function(id) {
		// init finder widget based on element id
		var widget = $("#" + id);
		
		// build OCAPI url form data attribute data-finder-options to get initial widget properties, like description, image, refinements
		var res = finder.buildOCAPIUrl(widget.data("finder-options")) + "/categories/product-finder?client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa&pretty_print=true";
		if(!res) {
			alert("Resource missing for finder widget #" + id);
			return false;
		}
		
		// make the request to retrieve the widget content
		$.ajax({
			url: res,
			dataType: "jsonp",
			async: true,
			data: {},
			jsonpCallback: "finder.setupWidget",
			cache: true
		}).done(function(response) {
			// nothing to do here
		}).fail(function(response) {
			// nothing to do here
		});
	},

	// sets up the widget initially using meta data retreived from the remote system
	setupWidget : function(data) {
		// select widget by data id
		// TODO get id from finder instance (make one instance per widget)
		var widget = $("#" + data.id);
		
		if(!widget) {
			alert("Element missing for finder widget #" + data.id);
			return false;
		}
		
		// fill the widget content
		widget.html("<h3>" + data.c_productFinderTitle + "</h3><div>" + data.c_productFinderDescription + "</div>");
		
		// the base category
		var baseCategory = ( data.c_productFinderCategoryID ? data.c_productFinderCategoryID : "root" );
		
		// make initial search request
		// use starting category from custom attribute (use root otherwise)
		var res = finder.buildOCAPIUrl(widget.data("finder-options")) + "/product_search?refine_1=cgid=" + ( baseCategory ) + "&client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa&pretty_print=true";
		$.ajax({
			url: res,
			dataType: "jsonp",
			async: true,
			data: {},
			jsonpCallback: "finder.updateWidget",
			cache: true
		}).done(function(response) {
			// nothing to do here
		}).fail(function(response) {
			// nothing to do here
		});
		
		// update status
		finder.updateStatus(widget.attr("id"), {refine_1:"cgid=" + baseCategory});
		
		// add the filter list as data element (to retrieve it with each update later)
		widget.data("finder-filters", data.c_productFinderFilters);
		
		// the container for the filters to append later
		widget.append("<div class='filters'/>");
		
		// the counter
		widget.append("<a class='btn btn-success disabled' target='_new'><span class='count'></span> <span class='text'>Updating...</span></a>");
	},

	// updates the widget with data retrieved from the remote system
	// might be incrementally be called as callback by the event handlers attached to the widget
	updateWidget : function(data) {
		// select widget by data id
		// TODO get id from finder instance (make one instance per widget)
		var widget = $("#product-finder");
		
		if(!widget) {
			alert("Element missing for finder widget #product-finder");
			return false;
		}
		
		// the filters to show
		var filters = widget.data("finder-filters");
		if(!filters || filters.length == 0) {
			alert("No filters configured for finder widget #" + widget.attr("id"));
			return false;
		}
		
		// the refine parameter index starts with 2, since index=1 is reserved for cgid
		var filterIdx = 2;
			
		// TODO update other filters or append if not exist yet (assuming, that existing filters doesn't have to be removed, since we use filters from initial category or root not from drill down category)
		// TODO order by finder filter list not by category refinement definition
		// this doesn't support category drilldown, since original set of refinements must be checked against the refinement list for the widget
		for(var i in filters) {
			// skip filter, if not configured for widget
			var candidate = data.refinements.filter(function(e,idx,arr){
				return e.attribute_id == filters[i] || e.attribute_id == "c_" + filters[i]
			});
			if(candidate.length != 1) {
				continue;
			}
			
			var ref = candidate[0];
			
			// skip filter, if already in widget
			if($("#" + widget.attr("id") + " select[name=" + ref.attribute_id + "]").length > 0) {
				continue;
			}
			
			// determine all options
			var options = [];
			options.push("<option value=''>All</option>");
			for(var j in ref.values) {
				var value = ref.values[j];
				options.push("<option value='" + value.value + "'>" + value.label + "</option>");
			}
			
			// create filter as select options
			$("#" + widget.attr("id") + " .filters").append("<div><label>" + ref.label + "</label><select name='" + ref.attribute_id + "' data-finder-idx='refine_" + filterIdx + "'>" + options.join("") + "</select></div>");
			
			// update the possible next refine parameter index
			filterIdx++;
		}
		
		// add event handler to refinements
		// TODO don't add handler if already existing
		$("#" + widget.attr("id") + " .filters select").one("change", function(e) {
			e.preventDefault();
			
			// set button to update status
			$("#" + widget.attr("id") + " .count").text("");
			$("#" + widget.attr("id") + " .text").text('Updating...');
			$("#" + widget.attr("id")).addClass("disabled");
			
			// update the status
			finder.updateStatus(widget.attr("id"), JSON.parse('{"'+$(this).data("finder-idx")+'":"' + $(this).attr("name") + '=' + $(this).val() +'"}'));
			
			// make the request
			var res = finder.appendOCAPIQueryParams(finder.buildOCAPIUrl(widget.data("finder-options")) + "/product_search?client_id=aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa&pretty_print=true", widget.data("finder-status"));
			$.ajax({
				url: res,
				dataType: "jsonp",
				async: true,
				data: {},
				jsonpCallback: "finder.updateWidget",
				cache: true
			}).done(function(response) {
				// nothing to do here
			}).fail(function(response) {
				// nothing to do here
			});
		})
		
		// update the button
		var btn = $("#" + widget.attr("id") + " a.btn");
		if( data.total == 0 ) {
			btn.removeClass("btn-success").addClass("btn-danger disabled");
			btn.attr("href", "#");
		} else {
			btn.removeClass("btn-danger disabled").addClass("btn-success");
			
			var shopUrl = finder.appendShopQueryParams(finder.buildShopUrl(widget.data("finder-options")) + "/Search-Show", widget.data("finder-status"));
			btn.attr("href", shopUrl);
		}
		
		// update the counter
		$("#" + widget.attr("id") + " .count").text(data.total);
		$("#" + widget.attr("id") + " .text").text('Product(s) found');
	},

	// updates the status of the finder, merges the given options object with the potentially already assigned options 
	updateStatus : function(id, options) {
		var current = $("#" + id).data("finder-status") || {};
		$("#" + id).data("finder-status", $.extend(current, options));
	},

	// utility function to build a base OCAPI url
	buildOCAPIUrl : function(options) {
		return "http://" + options.host + "/s/" + options.site + "/dw/shop/v12_6";
	},

	// utility function to build a base shop url
	buildShopUrl : function(options) {
		return "http://" + options.host + "/on/demandware.store/Sites-" + options.site + "-Site/default";
	},

	// appends OCAPI url parameters
	appendOCAPIQueryParams : function(url, params) {
		var paramList = [];
		for(var p in params) {
			paramList.push(p + '=' + params[p]);
		}
		return url + '&' + paramList.join('&');
	},

	// appends shop url parameters
	appendShopQueryParams : function(url, params) {
		var paramList = [];
		for(var p in params) {
			var param = params[p].split('=');
			if(!param[1]) {
				continue;
			}
			if(param[0] == 'cgid') {
				paramList.push('cgid=' + param[1]);
			}
			else if(param[0] == 'price') {
				var value = param[1].split('..');
				paramList.push('pmin=' + value[0].substr(1) + '&pmax=' + value[1].substring(0,value[1].indexOf(')')));
			}
			else {
				paramList.push(p.replace('refine_','prefn') + '=' + param[0].replace('c_','') + '&' + p.replace('refine_','prefv') + '=' + param[1]);
			}
		}
		return url + '?' + paramList.join('&');
	}
};

// bootstrap the widget
$(document).ready(function(){
	finder.init("product-finder");
});